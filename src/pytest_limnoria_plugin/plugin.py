#!/usr/bin/python3

###
# Copyright (c) 2002-2005, Jeremiah Fincher
# Copyright (c) 2011, James McCoy
# Copyright (c) 2021-2024, Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

########################################################################
#
# This pytest plugin is able to test Limnoria plugins using the normal
# testing tools that are part of Limnoria, but called via pytest rather
# than unittest, and therefore allowing pytest fixtures to be used.
#
# Note that the plugin is quite invasive due to a couple of quirks of
# Limnoria - namely the test IRC bot must be configured prior to the
# bot modules being imported and therefore prior to the plugin's
# test.py being loaded.
#
# The result of this is that all the supybot imports are hidden away
# inside functions.
#
#######################################################################
from __future__ import annotations

import atexit
import logging
from pathlib import Path
import shutil
import sys
import time

import pytest


# This will be the IRC configuration singleton if enabled
limnoria_plugin_tester: "LimnoriaPluginTester" | None = None


################################################################


def pytest_addoption(parser: pytest.Parser) -> None:
    group = parser.getgroup("limnoria_plugin")

    group.addoption(
        "--limnoria",
        action="store_true",
        default=False,
        dest="limnoria_enable",
        help="Enable this plugin.",
    )
    group.addoption(
        "--lim-no-clean",
        action="store_true",
        default=False,
        dest="lim_no_clean",
        help="Does not clean the various data/conf/logs"
        "directories before/after running tests.",
    )
    group.addoption(
        "--lim-timeout",
        action="store",
        type=float,
        metavar="N",
        dest="lim_timeout",
        help="Sets the timeout, in seconds, for tests to return responses.",
    )
    group.addoption(
        "--lim-no-network",
        action="store_true",
        default=False,
        dest="lim_nonetwork",
        help="Causes the network-based tests not to run.",
    )
    group.addoption(
        "--lim-no-setuid",
        action="store_true",
        default=False,
        dest="lim_nosetuid",
        help="Causes the tests based on a setuid executable not to run.",
    )
    group.addoption(
        "--lim-trace",
        action="store_true",
        default=False,
        dest="lim_trace",
        help="Traces all calls made. Beware copious amounts of output.",
    )
    group.addoption(
        "--lim-plugins-dir",
        action="append",
        dest="lim_pluginsdirs",
        metavar="DIR",
        default=[],
        help="Looks in in the given directory for plugins "
        "that were specified (multi-allowed).",
    )
    group.addoption(
        "--lim-disable-multiprocessing",
        action="store_true",
        dest="lim_disableMultiprocessing",
        help="Disables multiprocessing stuff.",
    )


################################################################


def pytest_sessionstart(session: pytest.Session) -> None:
    # FIXME would be nice to do this as a session fixture...
    # however, the configuration of the IRC world needs to be done prior
    # to the imports within the plugin test.py
    make_world(session)


def cleanup_handler() -> None:
    # logging.info("Cleaning up test config")
    if limnoria_plugin_tester and limnoria_plugin_tester.clean:
        shutil.rmtree(limnoria_plugin_tester.conf.supybot.directories.log())
        shutil.rmtree(limnoria_plugin_tester.conf.supybot.directories.conf())
        shutil.rmtree(limnoria_plugin_tester.conf.supybot.directories.data())


atexit.register(cleanup_handler)


################################################################


def make_world(session: pytest.Session) -> None:
    # this plugin is a bit too invasive; only enable if explicitly wanted
    if not session.config.getoption("limnoria_enable"):
        return

    # set test collection to look in the standard location
    session.config.addinivalue_line("python_files", "test.py")

    global limnoria_plugin_tester
    limnoria_plugin_tester = LimnoriaPluginTester(session.config)


class _TestLogFilter(logging.Filter):
    bads = [
        "No callbacks in",
        "Invalid channel database",
        "Exact error",
        "Invalid user dictionary",
        "because of noFlush",
        "Queuing NICK",
        "Queuing USER",
        "IgnoresDB.reload failed",
        "Starting log for",
        "Irc object for test dying",
        "Last Irc,",
    ]

    def filter(self, record: logging.LogRecord) -> bool:
        for bad in self.bads:
            if bad in record.msg:
                return False
        return True


class LimnoriaPluginTester:
    def __init__(self, config: pytest.Config) -> None:
        self.config = config
        self.option = self.config.getoption
        self.clean = True
        self.plugindirs: list[str] = []

        self.started = time.time()

        # We need to do this before we start bot services that
        # would import the configuration.
        self._create_config()
        self._create_world()
        self._configure()

    def _create_config(self) -> None:
        self.testconfdir = Path("test-conf")
        self.testconfdir.mkdir(exist_ok=True)
        self.registryFilename = self.testconfdir / "test.conf"

        logging.warning("Creating test config for bot: %s", self.registryFilename)

        base_dir = Path.cwd()
        with open(self.registryFilename, "w", encoding="UTF-8") as fd:
            fd.write(
                f"""\
supybot.directories.data: {base_dir}/test-data
supybot.directories.conf: {base_dir}/test-conf
supybot.directories.log: {base_dir}/test-logs
supybot.reply.whenNotCommand: True
supybot.log.stdout: False
supybot.log.stdout.level: ERROR
supybot.log.level: DEBUG
supybot.log.format: %%(levelname)s %%(message)s
supybot.log.plugins.individualLogfiles: False
supybot.protocols.irc.throttleTime: 0
supybot.reply.whenAddressedBy.chars: @
supybot.networks.test.server: should.not.need.this
supybot.networks.testnet1.server: should.not.need.this
supybot.networks.testnet2.server: should.not.need.this
supybot.networks.testnet3.server: should.not.need.this
supybot.nick: test
supybot.databases.users.allowUnregistration: True
"""
            )

        for conf in ("users", "channels", "networks", "ignores"):
            with open(self.testconfdir / f"{conf}.conf", "wb"):
                pass

    def _create_world(self) -> None:
        from supybot import registry  # type: ignore

        registry.open_registry(self.registryFilename)

        from supybot import log
        from supybot import conf

        conf.allowEval = True
        conf.supybot.flush.setValue(False)

        from supybot import test
        from supybot import utils
        from supybot import world

        world.startedAt = self.started

        self.conf = conf
        self.log = log
        self.test = test
        self.utils = utils
        self.world = world

        # Now that supybot.test can be safely imported, add this to the
        # parent namespace for use by the plugin
        import unittest

        class PytestPluginTestCase(test.PluginTestCase):  # type: ignore
            def __init__(self, methodName: str = "runTest") -> None:
                # avoid the supybot.test.PluginTestCase with pytest 8.2.1
                # since that will fail to collect tests
                # pylint: disable=non-parent-init-called, super-init-not-called
                self.originals: dict[str, registry.Group] = {}
                unittest.TestCase.__init__(self, methodName)

        import pytest_limnoria_plugin

        pytest_limnoria_plugin.PytestPluginTestCase = PytestPluginTestCase  # type: ignore

    def _configure(self) -> None:
        self._find_plugin_dir()

        self.log._logger.addFilter(_TestLogFilter())  # pylint: disable=protected-access

        self.clean = not self.option("lim_no_clean")

        self.world.disableMultiprocessing = self.option("lim_disableMultiprocessing")

        if self.option("lim_timeout"):
            self.test.timeout = self.option("lim_timeout")

        if self.option("lim_trace"):
            # pylint: disable=consider-using-with
            traceFilename = self.conf.supybot.directories.log.dirize("trace.log")
            fd = open(traceFilename, "w", encoding="UTF-8")
            sys.settrace(self.utils.gen.callTracer(fd))
            atexit.register(fd.close)
            atexit.register(lambda: sys.settrace(None))

        self.world.myVerbose = self.option("verbose")

        if self.option("lim_nonetwork"):
            self.test.network = False

        if self.option("lim_nosetuid"):
            self.test.setuid = False

        self.log.testing = True
        self.world.testing = True

        self.conf.supybot.directories.plugins.setValue(self.plugindirs)

    def _find_plugin_dir(self) -> None:
        # Find the plugin base directory to help limnoria plugin loader
        # locate the plugins to load them. The right answer is most likely
        # either ".." or "."

        if self.option("lim_pluginsdirs", None):
            # if it has been explicitly configured, don't try to do magic
            return

        targets = self.option("file_or_dir", [])[:]
        plugindirs = self.option("lim_pluginsdirs", [])[:]

        # add in an explicit "." item if nothing was given on the command
        # line - this will be then resolved to the actual plugin
        if not targets:
            targets.append(".")

        for file_or_dir in targets:
            # These might be a plugin directory or might be a test.py
            pth = Path(file_or_dir).resolve()
            if pth.is_dir():
                logging.warning("Looking for implicit %s/test.py", pth)
                pth = pth / "test.py"

            if pth.exists() and pth.name == "test.py":
                needed_plugdir = str(
                    ".." if pth.parent.name == "" else pth.parent.parent
                )
                if needed_plugdir not in plugindirs:
                    logging.warning("Adding %s to pluginsDirs", needed_plugdir)
                    plugindirs.append(needed_plugdir)

        self.plugindirs = plugindirs


################################################################


@pytest.fixture(scope="session")
def limnoria(request: pytest.FixtureRequest) -> LimnoriaPluginTester | None:
    # pylint: disable=unused-argument
    return limnoria_plugin_tester
