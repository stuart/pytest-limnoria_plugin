=========================================================
pytest-limnoria_plugin: test Limnoria plugins with pytest
=========================================================

.. .. image:: https://img.shields.io/pypi/v/pytest-limnoria_plugin.svg
..     :target: https://pypi.org/project/pytest-limnoria_plugin
..     :alt: PyPI version
..
.. .. image:: https://img.shields.io/pypi/pyversions/pytest-limnoria_plugin.svg
..     :target: https://pypi.org/project/pytest-limnoria_plugin
..     :alt: Python versions
..
.. .. image:: https://github.com/llimeht/pytest-limnoria_plugin/actions/workflows/main.yml/badge.svg
..     :target: https://github.com/llimeht/pytest-limnoria_plugin/actions/workflows/main.yml
..     :alt: See Build Status on GitHub Actions

Test limnoria IRC bot plugins with pytest

----

This `pytest`_ plugin is able to run unittests for plugins written for the
Limnoria/Supybot IRC bot framework.


Features
--------

* Supports existing `test.py` files from plugins with (almost) no changes
* Supports use of pytest fixtures in unit tests.


Installation
------------

You can install "pytest-limnoria_plugin" via `pip`_::

    $ pip install git+https://salsa.debian.org:stuart/pytest-limnoria_plugin.git

..     $ pip install pytest-limnoria_plugin


Usage
-----

* Instead of having the test classes inherit from `supybot.test.PluginTestCase`,
  inherit from `pytest_limnoria_plugin.PytestPluginTestCase`.
* When invoking `pytest` add the `--limnoria` commandline option (or add that
  via `pytest.ini` with `addopts = --limnoria`

Examples
--------

For the following layout::

    .
    ├── PluginA
    │   ├── __init__.py
    │   ├── config.py
    │   ├── plugin.py
    │   ├── README.rst
    │   └── test.py
    ├── PluginB
    │   ├── __init__.py
    │   ├── config.py
    │   ├── plugin.py
    │   ├── README.rst
    │   └── test.py
    └── ...


Test a plugin in a subdirectory::

    $ pytest -v --limnoria PluginA

Test several plugins in subdirectories::

    $ pytest -v --limnoria PluginA PluginB

Test the plugin in the current directory::

    $ cd PluginA
    $ pytest -v --limnoria .


Contributing
------------
Contributions are very welcome. There are currently no tests but
contribution of a test suite would be most welcome!

License
-------

Distributed under the terms of the `BSD-3`_ license, "pytest-limnoria_plugin" is free and open source software


Issues
------

If you encounter any problems, please `file an issue`_ along with a detailed description.

.. _`BSD-3`: https://opensource.org/licenses/BSD-3-Clause
.. _`file an issue`: https://github.com/llimeht/pytest-limnoria_plugin/issues
.. _`pytest`: https://github.com/pytest-dev/pytest
.. _`pip`: https://pypi.org/project/pip/
.. _`PyPI`: https://pypi.org/project
